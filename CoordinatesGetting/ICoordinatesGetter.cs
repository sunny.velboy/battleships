﻿using System;
namespace BattleShip.CoordinatesGetting
{
    public interface ICoordinatesGetter
    {
        public (int x, int y) GetCoordinatinates(int fieldSize);
        
    }
}
