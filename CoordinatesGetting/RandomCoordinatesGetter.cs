﻿using System;
namespace BattleShip.CoordinatesGetting
{
    public class RandomCoordinatesGetter : ICoordinatesGetter
    {
        private readonly Random random = new Random();
      
        public (int x, int y) GetCoordinatinates(int fieldSize)
        {
            int x = random.Next(0, fieldSize);
            int y = random.Next(0, fieldSize);
            return (x, y);
        }
    }
}
