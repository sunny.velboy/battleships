﻿using System;
using BattleShip.CoordinatesGetting;
using BattleShip.ShipsPlacing;

namespace BattleShip
{
    public class ParticipantFactory
    {
       
        public Participant CreateParticipant<TCoordinatesGetter, TShipsPlacer>(int size, Action participantLost = null)
            where TCoordinatesGetter : ICoordinatesGetter, new ()
            where TShipsPlacer : IShipsPlacer, new ()
        {
            var coordinatesGetter = new TCoordinatesGetter();
            var shipsPlacer = new TShipsPlacer();
            return new Participant(size, coordinatesGetter, shipsPlacer, participantLost);
        }
    }
}
